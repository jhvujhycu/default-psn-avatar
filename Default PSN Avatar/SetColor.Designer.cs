﻿namespace Default_PSN_Avatar
{
    partial class SetColor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetColor));
            this.setAsBgColor = new System.Windows.Forms.Button();
            this.colorDisplayBox = new System.Windows.Forms.PictureBox();
            this.BearerText = new System.Windows.Forms.Label();
            this.PickColor = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.hexCode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.colorDisplayBox)).BeginInit();
            this.SuspendLayout();
            // 
            // setAsBgColor
            // 
            this.setAsBgColor.Location = new System.Drawing.Point(21, 176);
            this.setAsBgColor.Name = "setAsBgColor";
            this.setAsBgColor.Size = new System.Drawing.Size(257, 47);
            this.setAsBgColor.TabIndex = 0;
            this.setAsBgColor.Text = "Set as Background Color";
            this.setAsBgColor.UseVisualStyleBackColor = true;
            this.setAsBgColor.Click += new System.EventHandler(this.setAsBgColor_Click);
            // 
            // colorDisplayBox
            // 
            this.colorDisplayBox.BackColor = System.Drawing.Color.White;
            this.colorDisplayBox.Location = new System.Drawing.Point(179, 88);
            this.colorDisplayBox.Name = "colorDisplayBox";
            this.colorDisplayBox.Size = new System.Drawing.Size(10, 10);
            this.colorDisplayBox.TabIndex = 1;
            this.colorDisplayBox.TabStop = false;
            // 
            // BearerText
            // 
            this.BearerText.AutoSize = true;
            this.BearerText.Location = new System.Drawing.Point(12, 9);
            this.BearerText.Name = "BearerText";
            this.BearerText.Size = new System.Drawing.Size(41, 13);
            this.BearerText.TabIndex = 2;
            this.BearerText.Text = "Bearer:";
            // 
            // PickColor
            // 
            this.PickColor.Location = new System.Drawing.Point(113, 101);
            this.PickColor.Name = "PickColor";
            this.PickColor.Size = new System.Drawing.Size(76, 23);
            this.PickColor.TabIndex = 4;
            this.PickColor.Text = "Picker";
            this.PickColor.UseVisualStyleBackColor = true;
            this.PickColor.Click += new System.EventHandler(this.PickColor_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(95, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Selected Color:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Created By SilicaAndPina";
            // 
            // hexCode
            // 
            this.hexCode.Location = new System.Drawing.Point(138, 130);
            this.hexCode.MaxLength = 6;
            this.hexCode.Name = "hexCode";
            this.hexCode.Size = new System.Drawing.Size(51, 20);
            this.hexCode.TabIndex = 7;
            this.hexCode.Text = "FFFFFF";
            this.hexCode.TextChanged += new System.EventHandler(this.hexCode_TextChanged);
            this.hexCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.hexCode_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(93, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Hex: #";
            // 
            // SetColor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(290, 235);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.hexCode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.PickColor);
            this.Controls.Add(this.BearerText);
            this.Controls.Add(this.colorDisplayBox);
            this.Controls.Add(this.setAsBgColor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SetColor";
            this.Text = "Set Profile Color";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SetColor_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.colorDisplayBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button setAsBgColor;
        private System.Windows.Forms.PictureBox colorDisplayBox;
        private System.Windows.Forms.Label BearerText;
        private System.Windows.Forms.Button PickColor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox hexCode;
        private System.Windows.Forms.Label label3;
    }
}