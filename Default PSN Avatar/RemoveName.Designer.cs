﻿namespace Default_PSN_Avatar
{
    partial class RemoveName
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RemoveName));
            this.BearerText = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DelName = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // BearerText
            // 
            this.BearerText.AutoSize = true;
            this.BearerText.Location = new System.Drawing.Point(12, 9);
            this.BearerText.Name = "BearerText";
            this.BearerText.Size = new System.Drawing.Size(44, 13);
            this.BearerText.TabIndex = 1;
            this.BearerText.Text = "Bearer: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Created By: SilicaAndPina";
            // 
            // DelName
            // 
            this.DelName.Location = new System.Drawing.Point(15, 183);
            this.DelName.Name = "DelName";
            this.DelName.Size = new System.Drawing.Size(274, 39);
            this.DelName.TabIndex = 3;
            this.DelName.Text = "Remove \"Real Name\"";
            this.DelName.UseVisualStyleBackColor = true;
            this.DelName.Click += new System.EventHandler(this.DelName_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Default_PSN_Avatar.Properties.Resources.BlankRealName;
            this.pictureBox1.Location = new System.Drawing.Point(16, 86);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(273, 61);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // RemoveName
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(297, 234);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.DelName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BearerText);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "RemoveName";
            this.Text = "Remove Name";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RemoveName_FormClosing);
            this.Load += new System.EventHandler(this.RemoveName_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label BearerText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button DelName;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}