﻿
namespace Default_PSN_Avatar
{
    partial class LoginForm
    {


        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
            this.LoginPage = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // LoginPage
            // 
            this.LoginPage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LoginPage.Location = new System.Drawing.Point(0, 0);
            this.LoginPage.MinimumSize = new System.Drawing.Size(20, 20);
            this.LoginPage.Name = "LoginPage";
            this.LoginPage.Size = new System.Drawing.Size(558, 646);
            this.LoginPage.TabIndex = 0;
            this.LoginPage.Url = new System.Uri("https://id.sonyentertainmentnetwork.com/id/management/#/p/psn_profile/list?entry=" +
        "psn_profile", System.UriKind.Absolute);
            this.LoginPage.WebBrowserShortcutsEnabled = false;
            this.LoginPage.Navigated += new System.Windows.Forms.WebBrowserNavigatedEventHandler(this.LoginPage_Navigated);
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(558, 646);
            this.Controls.Add(this.LoginPage);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LoginForm";
            this.Text = "Login to PSN";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LoginForm_FormClosing);
            this.Load += new System.EventHandler(this.LoginForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.WebBrowser LoginPage;
    }
}

