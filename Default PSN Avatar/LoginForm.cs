﻿using System;
using System.IO;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Runtime.InteropServices;

namespace Default_PSN_Avatar
{


    public partial class LoginForm : Form
    {

        [DllImport("wininet.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern bool InternetSetOption(IntPtr hInternet, int dwOption,IntPtr lpBuffer, int dwBufferLength);

        public LoginForm()
        {
            InitializeComponent();
        }


        public bool IsUrlGoneTo = false;


        private void LoginPage_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {

            if (IsUrlGoneTo == false)
            {
                if (LoginPage.Url.AbsoluteUri.Contains("client_id"))
                {
                    int i = LoginPage.Url.AbsoluteUri.IndexOf("&redirect_uri=");
                    string secondPartOfUrl = LoginPage.Url.AbsoluteUri.Substring(i);
                    string firstPartOfUrl = "https://id.sonyentertainmentnetwork.com/signin/?ui=pr&response_type=token&scope=openid%3Auser_id%20openid%3Aonline_id%20openid%3Actry_code%20openid%3Alang%20user%3Aaccount.communication.get%20kamaji%3Aget_account_hash%20oauth%3Amanage_user_auth_sessions%20openid%3Aacct_uuid%20user%3Aaccount.authentication.mode.get%20user%3Aaccount.phone.masked.get%20user%3Aaccount.notification.create%20openid%3Acontent_ctrl%20user%3Aaccount.subaccounts.get%20kamaji%3Aget_internal_entitlements%20user%3AverifiedAccount.get%20kamaji%3Aaccount_link_user_link_account%20kamaji%3Aactivity_feed_set_feed_privacy%20user%3Aaccount.identityMapper%20user%3Aaccount.email.create%20user%3Aaccount.emailVerification.get%20user%3Aaccount.tosua.update%20user:account.communication.update.gated%20user:account.communication.update%20user:account.profile.get%20user:account.profile.update";
                    /*
                     * Add the following permissions to the signin page: 
                     * 
                     * user:account.communication.update.gated 
                     * user:account.communication.update
                     * user:account.profile.get
                     * user:account.profile.update
                     * 
                     * Needed for changing real name.
                     */
                    string NewUrl = firstPartOfUrl + secondPartOfUrl;
                    LoginPage.Navigate(NewUrl);
                    IsUrlGoneTo = true;
                }

            }

            if (LoginPage.Url.AbsoluteUri.Contains("access_token"))
            {
                int i = LoginPage.Url.AbsoluteUri.IndexOf("access_token=") + 13;
                string Bearer = LoginPage.Url.AbsoluteUri.Substring(i);
                i = Bearer.IndexOf("&");
                Bearer = Bearer.Substring(0, i);

                LoginPage.Navigate("about:blank");

                SelectOptions selOption = new SelectOptions(Bearer);
                selOption.Show();
                this.Hide();
            }
        }


        private void LoginForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            //Disable Cookies
            var ptr = Marshal.AllocHGlobal(4);
            Marshal.WriteInt32(ptr, 3);
            InternetSetOption(IntPtr.Zero, 81, ptr, 4);
            Marshal.Release(ptr);

            int BrowserVer, RegVal;

            // get the installed IE version
            using (WebBrowser Wb = new WebBrowser())
                BrowserVer = Wb.Version.Major;

            // set the appropriate IE version
            if (BrowserVer >= 11)
                RegVal = 11001;
            else if (BrowserVer == 10)
                RegVal = 10001;
            else if (BrowserVer == 9)
                RegVal = 9999;
            else if (BrowserVer == 8)
                RegVal = 8888;
            else
                RegVal = 7000;

            // set the actual key
            using (RegistryKey Key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION", RegistryKeyPermissionCheck.ReadWriteSubTree))
                if (Key.GetValue(System.Diagnostics.Process.GetCurrentProcess().ProcessName + ".exe") == null)
                    Key.SetValue(System.Diagnostics.Process.GetCurrentProcess().ProcessName + ".exe", RegVal, RegistryValueKind.DWord);
            
        }

    }
}