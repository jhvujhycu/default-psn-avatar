﻿using System;
using System.Windows.Forms;

namespace Default_PSN_Avatar
{
    public partial class SelectOptions : Form
    {
        public string Bearer = "";

        public SelectOptions(string Token)
        {
            Bearer = Token;
            InitializeComponent();
            BearerText.Text = "Bearer: " + Bearer;
        }

        private void DefaultAvatar_Click(object sender, EventArgs e)
        {
            SetAvatar avaForm = new SetAvatar(Bearer);
            avaForm.Show();
            this.Hide();
        }

        private void SelectOptions_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void RemoveName_Click(object sender, EventArgs e)
        {
            RemoveName remForm = new RemoveName(Bearer);
            remForm.Show();
            this.Hide();
        }

        private void EndSession_Click(object sender, EventArgs e)
        {
            this.Hide();
            LoginForm lf = new LoginForm();
            lf.Show();

        }

        private void customColor_Click(object sender, EventArgs e)
        {
            SetColor colForm = new SetColor(Bearer);
            colForm.Show();
            this.Hide();
        }
    }
}
